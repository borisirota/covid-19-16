locales['ar'] = {
	"page-title": "فحص عوارض الكورونا",
	"main-title": "هل انت مصاب بالكورونا؟",
	"main-subtitle": "كورونا, إنفلونزا, رشح, أو حساسيّة",
	"table-title": "إختر العوارض للتّشديد على الأمراض",
	"disclaimer-header": "<strong>تنويه</strong> هذا الفحص لا يعتبر آداة تشخيص ولا يحل مكان الفحص الطبي. إحرص على مراجعة الطبيب في حال كنت تشتكي من احدى  العوارض!",
	"disclaimer-body": "هذه الآداة تهدف لمساعدتك بفهم المعلومات المزدحمة عن عوارض الكورونا الممكنة. إنها تظهر الاحتمال في ظهور العوارض في كل من 4 الامراض المذكورة. الأعراض التي تعاني منها قد تكون بسبب امراض أُخرى ليست مذكورة في الائحة.",
	"disclaimer-agree": "أَفهم وأوافق",
	"symptom": "عارض",
	"diseases": {
		"covid-19": "كورونا",
		"common cold": "رشح",
		"flu": "إنفلونزا",
		"allergies": "حساسية"
	},
	"symptoms": {
		"fever": "حمى",
		"shortness of breath": "ضيقة في التّنفس",
		"itchy or watery eyes": "حرق وادماع العيون",
		"dry cough": "سعال جاف",
		"headaches": "أوجاع بالرأس",
		"aches and pains": "توعكات وآلام",
		"sore throat": "وجع في الحلق",
		"fatigue": "تعب وإرهاق",
		"diarrhea": "إسهال",
		"runny or stuffy nose": "رشح الأنف",
		"sneezing": "عطس",
		"vomiting": "تقيّؤ",
		"worsening symptoms": "تفاقم في العوارض",
		"history of travel": "رحلات خارج البلاد في الآونة الأخيرة",
		"exposure to known covid-19 patient": "تعرض لمريض كورونا مشّخص"
	},
	"frequencies": {
		"common": "غالبًا",
		"rare": "نادرًا",
		"no": "كلا",
		"sometimes": "احيانًا",
		"mild": "خفيف",
	},
	"cta": [
		"معني في نسخة معدلة لمنطقتك والتي تحتوي تفاصيل مختبرات الفحص المحلية؟",
		"تبحث عن حلول لل-معالجة الطبية عن بعد- في أزمة الكورونا وللإسمترار في العنايّة بمرضى الكورونا المعافين والذين يعانوا من فشل في الرئات, القلب أو أعضاء أخرى؟",
		"هل لديك ملاحظات أو إقتراحات للتصليح أو التطوير؟"
	],
	"cta-button": "تواصل معنا",
	"tech-limits-title": "<strong>التكنولوجيا والمحدودية</strong>",
	"tech-limits-text": "حتّى اللحظة، ليس لدينا ما يكفي من المعلومات المتاحة لنتمكّن من بناء نموذج واقعي لتشخيص المرض بشكل دقيق بناء على الأعراض. عوضا عن ذلك، نستعمل تقريبا يستند إلى الإستدلال البايزي مع افتراض استقلالية الأعراض. حساب احتماليّة ظهور الأعراض لمرض معيّن تمّت بالاعتماد على دراسات مختلفة نشرت على يد CDD، NIH، WHO، Asthma and Allergy Foundation of America وغيرها من المصادر.",
	"tech-limits-team": "لدى فريق العمل سنين عديدة من الخبرة في - Machine learning, AI and Medical Triage. بسنة 2011 عرضوا مفهوم ال- <a href='https://en.wikipedia.org/wiki/Computer-aided_simple_triage' target='_blank'>computer-aided simple triage (CAST)</a> من خلال تطويرهم لحلول للتصوير الطبي التشخيصي في شركة <a href='http://rcadia.com/' target='_blank'>Rcadia</a>.",
	"about-us": "من نحن",
	"terms-of-use": "شروط الإستخدام",
	"privacy-policy": "سياسة الخصوصيّة",
	"copyrights": "حقوق النشر",
	"contact-us": "تواصل معنا"
};