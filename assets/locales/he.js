locales['he'] = {
	"page-title": "ביקרדיה - בדיקת תסמינים לקורונה",
	"main-title": "האם זה קורונה?",
	"main-subtitle": "קורונה, שפעת, הצטננות או אלרגיות",
	"table-title": "בחר תסמינים על מנת לסמן מחלות קשורות",
	"disclaimer-header": "<strong>הבהרה:</strong> זה אינו כלי אבחון רפואי. בדוק תמיד עם הרופא שלך אם יש לך תסמינים כלשהם!",
	"disclaimer-body": "המחשה זו נועדה לעזור לכם להבין את שטף המידע אודות תסמיני ווירוס הקורונה. היא מראה את הסבירות שהתסמינים נגרמים בגלל אחת מ- 4 מחלות. התסמינים שלך עשויים להיגרם כתוצאה ממצב אחר שאינו כלול בכלי.",
	"disclaimer-agree": "אני מבין ומאשר",
	"symptom": "תסמין",
	"diseases": {
		"covid-19": "קורונה",
		"common cold": "הצטננות",
		"flu": "שפעת",
		"allergies": "אלרגיות"
	},
	"symptoms": {
		"fever": "חום",
		"shortness of breath": "קוצר נשימה",
		"itchy or watery eyes": "עיניים דומעות או מגרדות",
		"dry cough": "שיעול יבש",
		"headaches": "כאבי ראש",
		"aches and pains": "מחושים או כאבים",
		"sore throat": "כאבי גרון",
		"fatigue": "תשישות",
		"diarrhea": "שלשולים",
		"runny or stuffy nose": "נזלת או גודש באף",
		"sneezing": "התעטשויות",
		"vomiting": "הקאות",
		"worsening symptoms": "החמרה של התסמינים",
		"history of travel": "היסטוריה של טיולים",
		"exposure to known covid-19 patient": "חשיפה לחולה קורונה"
	},
	"frequencies": {
		"common": "נפוץ",
		"rare": "לעתים נדירות",
		"no": "לא",
		"sometimes": "לפעמים",
		"mild": "קל",
	},
	"cta": [
		"מעוניין בגרסה מותאמת אישית עם כיסוי מעבדות הבדיקה המקומיות?",
		"מחפש פתרונות רפואה-מרחוק למשבר הקורונה והמשכיות הטיפול בשורדים עם נזק שיורי לריאות, לב ואיברים אחרים?",
		"יש לך משוב ורעיונות - כולל נתונים אמיתיים לשיפור מודל הנתונים?"
	],
	"cta-button": "צור קשר",
	"tech-limits-title": "<strong>טכנולוגיה ומגבלות<strong/>",
	"tech-limits-text": "נכון לעכשיו אין לנו גישה למספר נתונים גדול מספיק בכדי לבנות מודל ריאלי להערכה מדויקת של סבירות המחלה מהתסמינים. במקום זאת אנו משתמשים בקירוב גס המבוסס על הסקה בייסיאנית, בהנחה של אי-תלות בין התסמינים. הסבירות לתסמינים שניתנים למחלה מבוססת על נתונים שנאספו מפרסומים שונים על ידי CDC, NIH, WHO, Astma and Allergy Foundation of America ומקורות אחרים.",
	"tech-limits-team": "הצוות שלנו בעל ניסיון של שנים רבות ב- Machine learning, AI וטריאז' רפואי ובשנת 2011 הציג את המושג <a href='https://en.wikipedia.org/wiki/Computer-aided_simple_triage' target='_blank'>computer-aided simple triage (CAST)</a> תוך פיתוח פתרונות הדמיה רפואית בחברת <a href='http://rcadia.com/' target='_blank'>Rcadia</a>.",
	"about-us": "אודותינו",
	"terms-of-use": "תנאי שימוש",
	"privacy-policy": "מדיניות פרטיות",
	"copyrights": "זכויות יוצרים",
	"contact-us": "צור קשר"
};